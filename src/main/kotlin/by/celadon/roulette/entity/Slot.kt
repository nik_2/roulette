package by.celadon.roulette.entity

import javax.persistence.*

@Entity
@Table(name = "slots")
class Slot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id = 0
    @Column(name = "number")
    var number = 0
}