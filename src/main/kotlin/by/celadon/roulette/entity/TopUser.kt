package by.celadon.roulette.entity

class TopUser {
    var id = 0
    var username: String? = null
    var numberOfRounds = 0
    var averageOfBets = 0

    constructor()
    constructor(id: Int, username: String?, numberOfRounds: Int, averageOfBets: Int) {
        this.id = id
        this.username = username
        this.numberOfRounds = numberOfRounds
        this.averageOfBets = averageOfBets
    }
}