package by.celadon.roulette.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "bet")
class Bet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id = 0
    @Column(name = "amount")
    var amount = 0
    @Column(name = "slot_number")
    var slotNumber = 0
    @Column(name = "coefficient")
    var coefficient = 0
    @Column(name = "spin_number")
    var spinNumber = 0
    @JsonIgnore
    @ManyToOne(cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH])
    @JoinColumn(name = "user_id")
    var user: User? = null

    constructor()
    constructor(amount: Int, slotNumber: Int, coefficient: Int, spinNumber: Int, user: User?) {
        this.amount = amount
        this.slotNumber = slotNumber
        this.coefficient = coefficient
        this.spinNumber = spinNumber
        this.user = user
    }

}