package by.celadon.roulette.entity

import org.springframework.security.core.GrantedAuthority
import javax.persistence.*

@Table(name = "role")
@Entity
class Role : GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id = 0
    @Column(name = "name")
    var name: String? = null
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "roles")
    var users: Set<User>? = null

    override fun getAuthority(): String {
        return name!!
    }

    constructor()

    constructor(id: Int, name: String?) {
        this.id = id
        this.name = name
    }
}