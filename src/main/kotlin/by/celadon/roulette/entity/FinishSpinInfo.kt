package by.celadon.roulette.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "finish_spin_info")
class FinishSpinInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id = 0
    @Column(name = "finish_index")
    var finishIndex = 0
    @Column(name = "value")
    var value = 0
    @JsonIgnore
    @ManyToOne(cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE])
    @JoinColumn(name = "round_info_id")
    var roundInfo: RoundInfo? = null

    constructor()
    constructor(finishIndex: Int, value: Int, roundInfo: RoundInfo?) {
        this.finishIndex = finishIndex
        this.value = value
        this.roundInfo = roundInfo
    }

}