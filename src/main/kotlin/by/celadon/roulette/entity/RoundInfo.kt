package by.celadon.roulette.entity

import javax.persistence.*

@Entity
@Table(name = "round_info")
class RoundInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id = 0
    @Column(name = "spin_number")
    var spinNumber = 0
    @Column(name = "bet_number")
    var betNumber = 0
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "roundInfo")
    var finishSpins: List<FinishSpinInfo>? = null

}