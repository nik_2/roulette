package by.celadon.roulette.entity

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.*


@Entity
@Table(name = "user")
class User : UserDetails {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id = 0
    @Column(name = "username")
    private var username:@NotNull @NotEmpty(message = "Please, provide username") @Pattern(regexp = "[A-Za-z_\\d]{4,15}", message = "Use only numbers, letters and _") @Size(min = 4, max = 15, message = "Minimum chars - 4. Maximum chars - 15") String? = null
    @Column(name = "email")
    var email:@NotNull @Email(message = "Please, input correct email, for example: abc@xxx.xx") @NotEmpty(message = "Please, provide email") @Size(min = 5, max = 30, message = "Incorrect email. Please, use more then 4 chars and less 31") String? = null
    @Column(name = "first_name")
    var firstName: @NotNull @NotEmpty(message = "Please, provide first name") @Pattern(regexp = "[A-Z][a-z]+(-[A-Z][a-z]+)?", message = "Please, input correct first name, for example: Username or Username-Username") @Size(min = 2, max = 25, message = "Incorrect first name. Use more then 1 char and less then 26") String? = null
    @Column(name = "last_name")
    var lastName: @NotNull @NotEmpty(message = "Please, provide last name") @Pattern(regexp = "[A-Z][a-z]+(-[A-Z][a-z]+)?", message = "Please, input correct last name, for example: Username or Username-Username") @Size(min = 2, max = 25, message = "Incorrect last name. Use more then 1 char and less then 26") String? = null
    @Column(name = "password")
    private var password:@NotNull @NotEmpty(message = "Please, provide password") @Pattern(regexp = "[A-Za-z_\\d]{4,15}", message = "Use only numbers, letters and _") @Size(min = 4, max = 15, message = "Incorrect password. Please, use more then 3 chars and less then 16") String? = null
    @Column(name = "google_name")
    var googleName: String? = null
    @ManyToMany(fetch = FetchType.EAGER)
    var roles: Set<Role>? = null
    @Column(name = "last_visit")
    var lastVisit: LocalDateTime? = null
    @Column(name = "balance")
    var balance = 0
    @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL])
    var bets: MutableList<Bet>? = null


    constructor()

    constructor(username: String?, email: String?, firstName: String?, lastName: String?) {
        this.email = email
        this.firstName = firstName
        this.lastName = lastName
        this.username = username
    }

    override fun getAuthorities(): Collection<GrantedAuthority> {
        return roles!!
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun getUsername(): String {
        return username.toString()
    }

    fun setUsername(username: String?) {
        this.username = username
    }

    override fun getPassword(): String {
        return password.toString()
    }

    fun setPassword(password: String?) {
        this.password = password
    }

}