package by.celadon.roulette.entity.ajaxResponse

import by.celadon.roulette.entity.RoundInfo

class RoundAjaxResponse {
    var roundInfos: List<RoundInfo?>? = null
}