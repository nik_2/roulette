package by.celadon.roulette.entity.ajaxResponse

import by.celadon.roulette.entity.wrapper.BetWrapper

class SlotsAjaxResponse {
    var numbers: IntArray? = null
    var roundNumber = 0
    var lastSevenWinningSlots: List<Int?>? = null
    var coefficients: List<String?>? = null
    var lastTenBets: List<BetWrapper?>? = null
}