package by.celadon.roulette.entity.wrapper

class BetWrapper {
    var username: String? = null
    var slot = 0
    var coins = 0

    constructor(username: String?, slot: Int, coins: Int) {
        this.username = username
        this.slot = slot
        this.coins = coins
    }

    constructor()
}