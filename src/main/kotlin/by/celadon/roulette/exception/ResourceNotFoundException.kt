package by.celadon.roulette.exception


class ResourceNotFoundException
    : RuntimeException()