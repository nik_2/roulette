package by.celadon.roulette.util

import java.util.*
import kotlin.math.ceil

class CoefficientsGetter {
    fun findCoefficients(slots: IntArray?): List<String> {
        val coefficients: MutableList<String> = ArrayList()
        var totalAmount = 0.0
        for (slot in slots!!) {
            if (slot != -1) {
                totalAmount += slot.toDouble()
            }
        }
        for (i in 1..10) {
            var slotTotalAmount = 0.0
            var chance: Double
            for (slot in slots) {
                if (slot == i) {
                    slotTotalAmount += slot.toDouble()
                }
            }
            chance = slotTotalAmount / totalAmount
            if (chance != 0.0) {
                val coefficient = ceil(1 / chance).toInt()
                coefficients.add("Slot: $i; Coeff: $coefficient")
            }
        }
        coefficients.add("Slot: 0; Coeff: 100")
        return coefficients
    }
}