package by.celadon.roulette.util

import java.util.*

class FinishSlotGetter {
    fun getFinishSlot(slots: IntArray): Int {
        if (checkFinishSlotForReturnZero(slots)) {
            return 0
        }
        val chances = findChances(slots)
        val random = Math.random()
        if (random <= chances[0]) {
            return 1
        }
        if (random > chances[0] && random <= chances[1]) {
            return 2
        }
        if (random > chances[1] && random <= chances[2]) {
            return 3
        }
        if (random > chances[2] && random <= chances[3]) {
            return 4
        }
        if (random > chances[3] && random <= chances[4]) {
            return 5
        }
        if (random > chances[4] && random <= chances[5]) {
            return 6
        }
        if (random > chances[5] && random <= chances[6]) {
            return 7
        }
        if (random > chances[6] && random <= chances[7]) {
            return 8
        }
        if (random > chances[7] && random <= chances[8]) {
            return 9
        }
        return if (random > chances[8] && random <= chances[9]) {
            10
        } else 0
    }

    private fun checkFinishSlotForReturnZero(slots: IntArray): Boolean {
        var num1 = 0
        var num2 = 0
        var num3 = 0
        var num4 = 0
        var num5 = 0
        var num6 = 0
        var num7 = 0
        var num8 = 0
        var num9 = 0
        var num10 = 0
        for (slot in slots) {
            when (slot) {
                1 -> num1++
                2 -> num2++
                3 -> num3++
                4 -> num4++
                5 -> num5++
                6 -> num6++
                7 -> num7++
                8 -> num8++
                9 -> num9++
                10 -> num10++
            }
        }
        return num1 < 5 && num2 < 5 && num3 < 5 && num4 < 5 && num5 < 5 && num6 < 5 &&
                num7 < 5 && num8 < 5 && num9 < 5 && num10 < 5
    }

    private fun findChances(slots: IntArray): List<Double> {
        val chances: MutableList<Double> = ArrayList()
        var totalAmount = 0.0
        for (slot in slots) {
            if (slot != -1) {
                totalAmount += slot.toDouble()
            }
        }
        var tempChance = 0.0
        for (i in 1..10) {
            var slotTotalAmount = 0.0
            for (slot in slots) {
                if (slot == i) {
                    slotTotalAmount += slot.toDouble()
                }
            }
            tempChance += slotTotalAmount / totalAmount
            chances.add(tempChance)
        }
        return chances
    }
}