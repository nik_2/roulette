package by.celadon.roulette.service.slot

import by.celadon.roulette.entity.RoundInfo
import by.celadon.roulette.entity.wrapper.BetWrapper

interface SlotService {
    val currentSlots: IntArray?
    fun checkAndGetLastRoundInfo(): RoundInfo?
    fun addFinishSpinInfo(index: Int, value: Int)
    fun addRoundInfo()
    fun increaseSpinNumber()
    val currentRoundValue: Int
    val lastSevenWinningSlots: List<Int?>?
    fun getFinishSlot(slots: IntArray): Int
    fun getCoefficients(slots: IntArray?): List<String?>?
    fun payOut(finishSlot: Int)
    val lastTenBets: List<BetWrapper>
}