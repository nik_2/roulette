package by.celadon.roulette.service.slot

import by.celadon.roulette.dao.*
import by.celadon.roulette.entity.FinishSpinInfo
import by.celadon.roulette.entity.RoundInfo
import by.celadon.roulette.entity.wrapper.BetWrapper
import by.celadon.roulette.util.CoefficientsGetter
import by.celadon.roulette.util.FinishSlotGetter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class SlotServiceImpl @Autowired constructor(private val slotRepository: SlotRepository, private val roundInfoRepository: RoundInfoRepository,
                                             private val finishSpinInfoRepository: FinishSpinInfoRepository,
                                             private val betRepository: BetRepository,
                                             private val userRepository: UserRepository) : SlotService {
    override val currentSlots: IntArray?
        get() {
            val slots = slotRepository.findAllNumbers()
            val roundInfo = checkAndGetLastRoundInfo()
            val allIndexForCurrentRound = finishSpinInfoRepository.findAllIndexForCurrentRound(roundInfo)
            if (allIndexForCurrentRound != null) {
                for (index in allIndexForCurrentRound) {
                    slots!![index!!] = -1
                }
            }
            return slots
        }

    override fun checkAndGetLastRoundInfo(): RoundInfo? {
        val roundInfo = roundInfoRepository.findTopByOrderByIdDesc()
        return if (roundInfo == null) {
            roundInfoRepository.save(RoundInfo())
            roundInfoRepository.findTopByOrderByIdDesc()
        } else {
            roundInfo
        }
    }

    override fun addFinishSpinInfo(index: Int, value: Int) {
        val roundInfo = checkAndGetLastRoundInfo()
        val finishSpinInfo = FinishSpinInfo(index, value, roundInfo)
        finishSpinInfoRepository.save(finishSpinInfo)
    }

    override fun addRoundInfo() {
        roundInfoRepository.save(RoundInfo())
    }

    override fun increaseSpinNumber() {
        val roundInfo = roundInfoRepository.findTopByOrderByIdDesc()
        var spinNumber = roundInfo!!.spinNumber
        roundInfo.spinNumber = ++spinNumber
        roundInfoRepository.save(roundInfo)
    }

    override val currentRoundValue: Int
        get() {
            val roundInfo = roundInfoRepository.findTopByOrderByIdDesc()
            return roundInfo?.id!!
        }

    override val lastSevenWinningSlots: List<Int?>?
        get() {
            val roundInfo = roundInfoRepository.findTopByOrderByIdDesc()
            val winningSlots: MutableList<Int?>? = finishSpinInfoRepository.findAllValueForCurrentRound(roundInfo)
            val size = winningSlots!!.size
            if (size > 7) {
                return winningSlots.subList(size - 7, size)
            }
            while (winningSlots.size != 7) {
                winningSlots.add(0, -1)
            }
            return winningSlots
        }

    override fun getFinishSlot(slots: IntArray): Int {
        val finishSlotGetter = FinishSlotGetter()
        return finishSlotGetter.getFinishSlot(slots)
    }

    override fun getCoefficients(slots: IntArray?): List<String?>? {
        val coefficientsGetter = CoefficientsGetter()
        return coefficientsGetter.findCoefficients(slots)
    }

    override fun payOut(finishSlot: Int) {
        val spinNumber = finishSpinInfoRepository.findTopByOrderByIdDesc().id + 1
        val winningBets = betRepository.findAllBySpinNumberAndSlotNumber(spinNumber, finishSlot)
        for (winningBet in winningBets!!) {
            val user = winningBet?.user
            user!!.balance = user.balance + winningBet.amount * winningBet.coefficient
            userRepository.save(user)
        }
    }

    override val lastTenBets: List<BetWrapper>
        get() {
            val spinNumber = finishSpinInfoRepository.findTopByOrderByIdDesc().id + 1
            var bets = betRepository.findAllBySpinNumber(spinNumber)
            val betWrappers: MutableList<BetWrapper> = ArrayList()
            val size = bets!!.size
            if (size > 10) {
                bets = bets.subList(size - 10, size)
            }
            for (bet in bets) {
                val betWrapper = BetWrapper(bet!!.user!!.username, bet.slotNumber, bet.amount)
                betWrappers.add(betWrapper)
            }
            while (betWrappers.size != 10) {
                betWrappers.add(0, BetWrapper())
            }
            return betWrappers
        }

}