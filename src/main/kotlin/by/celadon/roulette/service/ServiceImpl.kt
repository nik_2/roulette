package by.celadon.roulette.service

import by.celadon.roulette.dao.FinishSpinInfoRepository
import by.celadon.roulette.dao.RoundInfoRepository
import by.celadon.roulette.dao.UserRepository
import by.celadon.roulette.entity.RoundInfo
import by.celadon.roulette.entity.TopUser
import org.springframework.beans.factory.annotation.Autowired
import java.util.*

@org.springframework.stereotype.Service
class ServiceImpl @Autowired constructor(private val userRepository: UserRepository,
                                         private val finishSpinInfoRepository: FinishSpinInfoRepository,
                                         private val roundInfoRepository: RoundInfoRepository) : Service {
    override val topUsers: List<TopUser>
        get() {
            var users = userRepository.findAllUsers()
            if (users!!.size > 10) {
                users = users.subList(0, 10)
            }
            val topUsers: MutableList<TopUser> = ArrayList()
            for (user in users) {
                val rounds = HashSet<Int>()
                for (bet in user?.bets!!) {
                    val finishSpinInfo = finishSpinInfoRepository.findAllById(bet.spinNumber)
                    if (finishSpinInfo == null) {
                        val previousSpinInfo = finishSpinInfoRepository.findAllById(bet.spinNumber - 1)
                        if (previousSpinInfo != null) {
                            if (previousSpinInfo.value != 0) {
                                rounds.add(previousSpinInfo.roundInfo!!.id)
                            } else {
                                rounds.add(previousSpinInfo.roundInfo!!.id + 1)
                            }
                        }
                    } else {
                        rounds.add(finishSpinInfo.roundInfo!!.id)
                    }
                }
                val numberOfRounds = rounds.size
                val numberOfBets = user.bets!!.size
                var averageOfBets = 0
                if( numberOfBets != 0){
                    averageOfBets = numberOfBets / numberOfRounds
                }
                val topUser = TopUser(user.id, user.username, rounds.size, averageOfBets)
                topUsers.add(topUser)
            }
            while (topUsers.size != 10) {
                topUsers.add(TopUser())
            }
            return topUsers
        }

    override val roundInfos: List<RoundInfo?>
        get() {
            val roundInfos = roundInfoRepository.findAll()
            while (roundInfos.size < 10) {
                roundInfos.add(0, RoundInfo())
            }
            return roundInfos
        }

}