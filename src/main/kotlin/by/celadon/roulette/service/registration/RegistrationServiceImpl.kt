package by.celadon.roulette.service.registration

import by.celadon.roulette.dao.UserRepository
import by.celadon.roulette.entity.Role
import by.celadon.roulette.entity.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class RegistrationServiceImpl

 @Autowired constructor(private val userRepository: UserRepository, private val bCryptPasswordEncoder: BCryptPasswordEncoder) : RegistrationService {
    override fun checkNewUserData(user: User?): String {
        val userByEmail = userRepository.findFirstByEmail(user?.email)
        val userByUsername = userRepository.findFirstByUsername(user?.username)
        if (userByEmail != null && userByEmail.password != null && userByUsername != null) {
            return "usernameAndEmailError"
        }
        if (userByEmail != null && userByEmail.password != null) {
            return "emailError"
        }
        if (userByUsername != null) {
            return "usernameError"
        }
        if (userByEmail != null && userByEmail.password == null) {
            userByEmail.firstName = user?.firstName
            userByEmail.lastName = user?.lastName
            userByEmail.setUsername(user?.username)
            userByEmail.setPassword(bCryptPasswordEncoder.encode(user?.password))
            userRepository.save(userByEmail)
            return "success"
        }
        user?.roles = setOf(Role(1, "ROLE_USER"))
        user?.setPassword(bCryptPasswordEncoder.encode(user.password))
        user?.balance = 500
        if (user != null) {
            userRepository.save(user)
        }
        return "success"
    }

}