package by.celadon.roulette.service.registration

import by.celadon.roulette.entity.User

interface RegistrationService {
    fun checkNewUserData(user: User?): String
}