package by.celadon.roulette.service.user

import by.celadon.roulette.dao.BetRepository
import by.celadon.roulette.dao.FinishSpinInfoRepository
import by.celadon.roulette.dao.RoundInfoRepository
import by.celadon.roulette.dao.UserRepository
import by.celadon.roulette.entity.Bet
import by.celadon.roulette.entity.User
import by.celadon.roulette.validator.AmountValidator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.stereotype.Service
import java.security.Principal
import java.time.LocalDateTime

@Service
class UserServiceImpl @Autowired constructor(private val userRepository: UserRepository,
                                             private val betRepository: BetRepository,
                                             private val finishSpinInfoRepository: FinishSpinInfoRepository,
                                             private val roundInfoRepository: RoundInfoRepository) : UserService {
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(usernameOrMail: String): UserDetails {
        val user = userRepository.findFirstByUsernameOrEmail(usernameOrMail, usernameOrMail)
                ?: throw UsernameNotFoundException("User not found")
        user.lastVisit = LocalDateTime.now()
        userRepository.save(user)
        return user
    }

    override fun getCurrentUser(principal: Principal): User? {
        val user: User?
        user = if (principal is OAuth2Authentication) {
            val authentication = principal.userAuthentication
            val details = authentication.details as Map<String, String>
            findUserByEmail(details["email"])
        } else {
            findUserByUsername(principal.name)
        }
        return user
    }

    override fun findUserByEmail(email: String?): User? {
        return userRepository.findByEmail(email)
    }

    override fun findUserByUsername(username: String?): User? {
        return userRepository.findByUsername(username)
    }

    override fun checkAmount(amount: String?, user: User?): String {
        return if (AmountValidator.validateAmount(amount)) {
            if (amount!!.toInt() <= user!!.balance) {
                "success"
            } else {
                "errorBalance"
            }
        } else {
            "errorValidation"
        }
    }

    override fun placeBet(amount: String?, slot: String?, coeff: String?, roundNumber: String?, user: User?) {
        val spinNumber = finishSpinInfoRepository.findTopByOrderByIdDesc().id + 1
        val bet = Bet(amount!!.toInt(), slot!!.toInt(), coeff!!.toInt(), spinNumber, user)
        user!!.balance = user.balance - amount.toInt()
        betRepository.save(bet)
        userRepository.save(user)
        val optionalRoundInfo = roundInfoRepository.findById(roundNumber!!.toInt())
        if (optionalRoundInfo.isPresent) {
            val roundInfo = optionalRoundInfo.get()
            var betNumber = roundInfo.betNumber
            roundInfo.betNumber = ++betNumber
            roundInfoRepository.save(roundInfo)
        }
    }

    override fun getMyBets(user: User?): List<Bet?>? {
        var bets = user?.bets
        val size = bets!!.size
        if (size > 10) {
            bets = bets.subList(size - 10, size)
        }
        while (bets.size != 10) {
            bets.add(0, Bet())
        }
        return bets
    }

}