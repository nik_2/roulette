package by.celadon.roulette.service.user

import by.celadon.roulette.entity.Bet
import by.celadon.roulette.entity.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import java.security.Principal

interface UserService : UserDetailsService {
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(s: String): UserDetails

    fun getCurrentUser(principal: Principal): User?
    fun findUserByEmail(email: String?): User?
    fun findUserByUsername(username: String?): User?
    fun checkAmount(amount: String?, user: User?): String
    fun placeBet(amount: String?, slot: String?, coeff: String?, roundNumber: String?, user: User?)
    fun getMyBets(user: User?): List<Bet?>?
}