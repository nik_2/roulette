package by.celadon.roulette.service

import by.celadon.roulette.entity.RoundInfo
import by.celadon.roulette.entity.TopUser

interface Service {
    val topUsers: List<TopUser>
    val roundInfos: List<RoundInfo?>
}