package by.celadon.roulette.advice

import by.celadon.roulette.exception.ResourceNotFoundException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.ModelAndView

@ControllerAdvice
class ExceptionInterceptor {
    @ExceptionHandler(ResourceNotFoundException::class)
    fun handleError404(): ModelAndView {
        return ModelAndView("error/404")
    }
}