package by.celadon.roulette.validator

import by.celadon.roulette.constant.PatternConst.VALUE_PATTERN

object AmountValidator {
    fun validateAmount(enterValue: String?): Boolean {
        return enterValue!!.matches(Regex(VALUE_PATTERN)) && enterValue.length <= 10 && enterValue.length >= 1
    }
}