package by.celadon.roulette.config

import by.celadon.roulette.dao.UserRepository
import by.celadon.roulette.service.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.client.OAuth2ClientContext
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.Filter


@Configuration
@EnableWebSecurity
@EnableOAuth2Client
class SecurityConfig
@Autowired constructor(private val userService: UserService, private val oAuth2ClientContext: OAuth2ClientContext,
                       private val userRepository: UserRepository) : WebSecurityConfigurerAdapter() {
    @Bean
    fun passwordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun oAuth2ClientFilterRegistration(oAuth2ClientContextFilter: OAuth2ClientContextFilter): FilterRegistrationBean<OAuth2ClientContextFilter> {
        val registration = FilterRegistrationBean<OAuth2ClientContextFilter>()
        registration.filter = oAuth2ClientContextFilter
        registration.order = -100
        return registration
    }

    private fun ssoFilter(): Filter {
        val googleFilter = OAuth2ClientAuthenticationProcessingFilter("/login/google")
        val googleTemplate = OAuth2RestTemplate(google(), oAuth2ClientContext)
        googleFilter.setRestTemplate(googleTemplate)
        val tokenServices = CustomUserInfoTokenServices(googleResource().userInfoUri, google().clientId)
        tokenServices.setRestTemplate(googleTemplate)
        googleFilter.setTokenServices(tokenServices)
        tokenServices.setUserRepository(userRepository)
        return googleFilter
    }

    @Bean
    @ConfigurationProperties("security.oauth2.client")
    fun google(): AuthorizationCodeResourceDetails {
        return AuthorizationCodeResourceDetails()
    }

    @Bean
    @ConfigurationProperties("security.oauth2.resource")
    fun googleResource(): ResourceServerProperties {
        return ResourceServerProperties()
    }

    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userService)
                .passwordEncoder(passwordEncoder())
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/login**", "/registration").not().authenticated()
                .antMatchers("/user/**").hasRole("USER")
                .antMatchers("/**", "/css/**",
                        "/img/**", "/js/**", "/fonts/**", "/vendor/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(ssoFilter(), UsernamePasswordAuthenticationFilter::class.java)
                .formLogin()
                .usernameParameter("username")
                .usernameParameter("mail")
                .loginPage("/login")
                .defaultSuccessUrl("/")
                .permitAll()
                .and()
                .rememberMe()
                .alwaysRemember(true)
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied")
    }

}