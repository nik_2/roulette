package by.celadon.roulette.config

import by.celadon.roulette.dao.UserRepository
import by.celadon.roulette.entity.Role
import by.celadon.roulette.entity.User
import org.apache.commons.logging.LogFactory
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor
import org.springframework.boot.autoconfigure.security.oauth2.resource.FixedAuthoritiesExtractor
import org.springframework.boot.autoconfigure.security.oauth2.resource.FixedPrincipalExtractor
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException
import org.springframework.security.oauth2.client.OAuth2RestOperations
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.OAuth2Request
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices
import org.springframework.util.Assert
import java.time.LocalDateTime
import java.util.*

class CustomUserInfoTokenServices
(private val userInfoEndpointUrl: String, private val clientId: String) : ResourceServerTokenServices {
    private val logger = LogFactory.getLog(this.javaClass)
    private var restTemplate: OAuth2RestOperations? = null
    private var tokenType = "Bearer"
    private var authoritiesExtractor: AuthoritiesExtractor = FixedAuthoritiesExtractor()
    private var principalExtractor: PrincipalExtractor = FixedPrincipalExtractor()
    private var userRepository: UserRepository? = null

    fun setUserRepository(userRepository: UserRepository?) {
        this.userRepository = userRepository
    }

    fun setTokenType(tokenType: String) {
        this.tokenType = tokenType
    }

    fun setRestTemplate(restTemplate: OAuth2RestOperations?) {
        this.restTemplate = restTemplate
    }

    fun setAuthoritiesExtractor(authoritiesExtractor: AuthoritiesExtractor) {
        Assert.notNull(authoritiesExtractor, "AuthoritiesExtractor must not be null")
        this.authoritiesExtractor = authoritiesExtractor
    }

    fun setPrincipalExtractor(principalExtractor: PrincipalExtractor) {
        Assert.notNull(principalExtractor, "PrincipalExtractor must not be null")
        this.principalExtractor = principalExtractor
    }

    @Throws(AuthenticationException::class, InvalidTokenException::class)
    override fun loadAuthentication(accessToken: String): OAuth2Authentication {
        val map = getMap(userInfoEndpointUrl, accessToken)
        if (map.containsKey("sub")) {
            val googleName = map["name"] as String?
            val googleEmail = map["email"] as String?
            var user = userRepository!!.findByEmail(googleEmail)
            if (user == null) {
                user = User()
                user.email = googleEmail
                user.googleName = googleName
                user.balance = 500
                user.roles = setOf(Role(1, "ROLE_USER"))
            }
            val userGoogleName = user.googleName
            if (userGoogleName == null || userGoogleName != googleName) {
                user.googleName = googleName
            }
            user.lastVisit = LocalDateTime.now()
            userRepository!!.save(user)
        }
        if (map.containsKey("error")) {
            if (logger.isDebugEnabled) {
                logger.debug("userinfo returned error: " + map["error"])
                throw InvalidTokenException(accessToken)
            }
        }
        return extractAuthentication(map)
    }

    private fun extractAuthentication(map: Map<String, Any?>): OAuth2Authentication {
        val principal = getPrincipal(map)
        val authorities = authoritiesExtractor.extractAuthorities(map)
        val request = OAuth2Request(null, clientId, null, true, null, null, null, null, null)
        val token = UsernamePasswordAuthenticationToken(principal, "N/A", authorities)
        token.details = map
        return OAuth2Authentication(request, token)
    }

    protected fun getPrincipal(map: Map<String, Any?>?): Any {
        val principal = principalExtractor.extractPrincipal(map)
        return principal ?: "unknown"
    }

    override fun readAccessToken(accessToken: String): OAuth2AccessToken {
        throw UnsupportedOperationException("Not supported: read access token")
    }

    private fun getMap(path: String, accessToken: String): Map<String, Any?> {
        if (logger.isDebugEnabled) {
            logger.debug("Getting user info from: $path")
        }
        return try {
            var restTemplate = restTemplate
            if (restTemplate == null) {
                val resource = BaseOAuth2ProtectedResourceDetails()
                resource.clientId = clientId
                restTemplate = OAuth2RestTemplate(resource)
            }
            val existingToken = restTemplate.oAuth2ClientContext.accessToken
            if (existingToken == null || accessToken != existingToken.value) {
                val token = DefaultOAuth2AccessToken(accessToken)
                token.tokenType = tokenType
                restTemplate.oAuth2ClientContext.accessToken = token
            }
            restTemplate.getForEntity(path, MutableMap::class.java).body as Map<String, String>
        } catch (var6: Exception) {
            logger.warn("Could not fetch user details: " + var6.javaClass + ", " + var6.message)
            Collections.singletonMap<String, Any?>("error", "Could not fetch user details")
        }
    }

}