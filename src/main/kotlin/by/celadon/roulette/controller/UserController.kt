package by.celadon.roulette.controller

import by.celadon.roulette.entity.Bet
import by.celadon.roulette.entity.ajaxResponse.BalanceAjaxResponse
import by.celadon.roulette.entity.ajaxResponse.PlaceBetAjaxResponse
import by.celadon.roulette.service.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.security.Principal
import java.util.*

@Controller
@RequestMapping("/user")
class UserController @Autowired constructor(private val userService: UserService) {
    @GetMapping("/profile")
    fun showProfile(): String {
        return "user/profile"
    }

    @GetMapping("/getBalance")
    @ResponseBody
    fun getBalance(principal: Principal): BalanceAjaxResponse {
        val user = userService.getCurrentUser(principal)
        val balanceAjaxResponse = BalanceAjaxResponse()
        if (user != null) {
            balanceAjaxResponse.balance = user.balance
        }
        return balanceAjaxResponse
    }

    @PostMapping("/placeBet")
    @ResponseBody
    fun placeBet(@RequestBody data: HashMap<String?, String?>, principal: Principal): PlaceBetAjaxResponse {
        val placeBetAjaxResponse = PlaceBetAjaxResponse()
        val user = userService.getCurrentUser(principal)
        when (val resultOfCheck = userService.checkAmount(data["amount"], user)) {
            "success" -> userService.placeBet(data["amount"],
                    data["slot"],
                    data["coeff"],
                    data["roundNumber"],
                    user)
            "errorValidation", "errorBalance" -> placeBetAjaxResponse.errorParam = resultOfCheck
        }
        return placeBetAjaxResponse
    }

    @GetMapping("/getMyBets")
    @ResponseBody
    fun getMyBets(principal: Principal): List<Bet?>? {
        val user = userService.getCurrentUser(principal)
        return userService.getMyBets(user)
    }

}