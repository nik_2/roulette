package by.celadon.roulette.controller

import by.celadon.roulette.entity.User
import by.celadon.roulette.service.registration.RegistrationService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import javax.validation.Valid


@Controller
class RegistrationController
(private val registrationService: RegistrationService) {
    @GetMapping("/registration")
    fun getRegistrationPage(model: Model): String {
        val user: User?
        user = User()
        user.setUsername("")
        model.addAttribute("user", user)
        return "guest/register"
    }

    @PostMapping("/registration")
    fun checkRegistrationInfo(@ModelAttribute("user") user: @Valid User?, bindingResult: BindingResult, model: Model): String {
        if (bindingResult.hasErrors()) {
            return "guest/register"
        }
        return when (registrationService.checkNewUserData(user)) {
            "usernameError" -> {
                model.addAttribute("usernameError", "User with this username already exists")
                "guest/register"
            }
            "emailError" -> {
                model.addAttribute("emailError", "User with this email already exists")
                "guest/register"
            }
            "usernameAndEmailError" -> {
                model.addAttribute("emailAndUsernameError", "User with this username and email already exists")
                "guest/register"
            }
            else -> "redirect:/login"
        }
    }

}