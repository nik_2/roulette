package by.celadon.roulette.controller

import by.celadon.roulette.entity.ajaxResponse.FinishSlotAjaxResponse
import by.celadon.roulette.entity.ajaxResponse.RoundAjaxResponse
import by.celadon.roulette.entity.ajaxResponse.SlotsAjaxResponse
import by.celadon.roulette.entity.ajaxResponse.TopAjaxResponse
import by.celadon.roulette.service.Service
import by.celadon.roulette.service.slot.SlotService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/")
class MainController @Autowired constructor(private val slotService: SlotService, private val service: Service) {
    @GetMapping(value = ["/"])
    fun showMainPage(): String {
        return "redirect:/roulette"
    }

    @GetMapping("/roulette")
    fun showRoulette(): String {
        return "roulette"
    }

    @GetMapping(value = ["/getSlots"])
    @ResponseBody
    fun getSlots(@RequestParam("index") index: Int,
                 @RequestParam("value") value: Int,
                 @RequestParam("flag") flag: Boolean): SlotsAjaxResponse {
        if (flag) {
            slotService.addFinishSpinInfo(index, value)
        }
        if (value == 0) {
            slotService.addRoundInfo()
        }
        val slots = slotService.currentSlots
        val result = SlotsAjaxResponse()
        result.numbers = slots
        result.roundNumber = slotService.currentRoundValue
        result.lastSevenWinningSlots = slotService.lastSevenWinningSlots
        result.lastTenBets = slotService.lastTenBets
        result.coefficients = slotService.getCoefficients(slots)
        return result
    }

    @PostMapping(value = ["/getFinishSlots"])
    @ResponseBody
    fun getFinishSlot(@RequestBody slots: IntArray): FinishSlotAjaxResponse {
        slotService.increaseSpinNumber()
        val response = FinishSlotAjaxResponse()
        val finishSlot = slotService.getFinishSlot(slots)
        slotService.payOut(finishSlot)
        response.count = finishSlot
        return response
    }

    @GetMapping(value = ["/accessDenied"])
    fun viewAccessDenied(): String {
        return "error/access-denied"
    }

    @GetMapping(value = ["/top"])
    fun showTopUsersPage(): String {
        return "top"
    }

    @get:ResponseBody
    @get:GetMapping(value = ["/getTopUsers"])
    val topUsers: TopAjaxResponse
        get() {
            val topAjaxResponse = TopAjaxResponse()
            topAjaxResponse.topUsers = service.topUsers
            return topAjaxResponse
        }

    @GetMapping(value = ["/participants"])
    fun showNumberOfParticipantsPage(): String {
        return "info"
    }

    @get:ResponseBody
    @get:GetMapping(value = ["/getRoundInfo"])
    val roundInfo: RoundAjaxResponse
        get() {
            val roundAjaxResponse = RoundAjaxResponse()
            roundAjaxResponse.roundInfos = service.roundInfos
            return roundAjaxResponse
        }

}