package by.celadon.roulette.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import java.security.Principal


@Controller
class LoginController {
    @GetMapping("/login")
    fun getLoginPage(principal: Principal?): String {
        return if (principal == null) "guest/login" else "redirect:/accessDenied"
    }
}