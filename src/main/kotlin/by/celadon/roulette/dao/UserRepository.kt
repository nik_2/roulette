package by.celadon.roulette.dao

import by.celadon.roulette.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface UserRepository : JpaRepository<User?, Int?> {
    fun findByUsername(username: String?): User?
    fun findFirstByUsername(username: String?): User?
    fun findFirstByEmail(email: String?): User?
    fun findFirstByUsernameOrEmail(username: String?, email: String?): User?
    fun findByEmail(email: String?): User?

    @Query(value = "select u from User u order by u.bets.size desc")
    fun findAllUsers(): List<User?>?
}