package by.celadon.roulette.dao

import by.celadon.roulette.entity.Slot
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface SlotRepository : JpaRepository<Slot?, Int?> {
    @Query(value = "select number from Slot")
    fun findAllNumbers(): IntArray?
}