package by.celadon.roulette.dao

import by.celadon.roulette.entity.Role
import org.springframework.data.jpa.repository.JpaRepository

interface RoleRepository : JpaRepository<Role?, Int?> {
    fun findByName(name: String?): Set<Role?>?
}