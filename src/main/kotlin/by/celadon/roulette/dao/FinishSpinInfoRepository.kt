package by.celadon.roulette.dao

import by.celadon.roulette.entity.FinishSpinInfo
import by.celadon.roulette.entity.RoundInfo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface FinishSpinInfoRepository : JpaRepository<FinishSpinInfo?, Int?> {
    @Query(value = "select finishIndex from FinishSpinInfo where roundInfo = :roundInfo")
    fun findAllIndexForCurrentRound(@Param("roundInfo") roundInfo: RoundInfo?): ArrayList<Int?>?

    @Query(value = "select value from FinishSpinInfo where roundInfo = :roundInfo")
    fun findAllValueForCurrentRound(@Param("roundInfo") roundInfo: RoundInfo?): ArrayList<Int?>?

    fun findTopByOrderByIdDesc(): FinishSpinInfo
    fun findAllById(id: Int): FinishSpinInfo?
}