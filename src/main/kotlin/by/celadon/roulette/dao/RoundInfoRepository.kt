package by.celadon.roulette.dao

import by.celadon.roulette.entity.RoundInfo
import org.springframework.data.jpa.repository.JpaRepository

interface RoundInfoRepository : JpaRepository<RoundInfo?, Int?> {
    fun findTopByOrderByIdDesc(): RoundInfo?
}