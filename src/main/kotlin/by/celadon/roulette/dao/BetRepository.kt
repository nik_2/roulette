package by.celadon.roulette.dao

import by.celadon.roulette.entity.Bet
import org.springframework.data.jpa.repository.JpaRepository

interface BetRepository : JpaRepository<Bet?, Int?> {
    fun findAllBySpinNumberAndSlotNumber(spinNumber: Int, slotNumber: Int): List<Bet?>?
    fun findAllBySpinNumber(spinNumber: Int): List<Bet?>?
}