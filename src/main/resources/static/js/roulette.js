function getFinishSlotAjax() {
    $.ajax({
        url: 'getFinishSlots',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(options),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            result = data.count;
            $("#roundNumber").text(roundNumber);
        }
    });
}

function getBalance() {
    return $.ajax({
        url: 'user/getBalance',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            balance = data.balance;
            $("#balance").text('Balance: ' + balance);
        }
    });
}

function placeBet() {
    amount = document.getElementById("amount").value;
    if (amount === '') {
        amount = 0;
    }
    bet = document.getElementById("slot").value;
    bet = bet.replace('Slot: ', '');
    bet = bet.replace('; Coeff: ', ' ');
    bet = bet.split(' ');
    slot = bet[0];
    coeff = bet[1];
    data = {amount: amount, slot: slot, coeff: coeff, roundNumber: roundNumber};
    return $.ajax({
        url: 'user/placeBet',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(data),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            $.when(getSlotsAjax()).done(function () {
                showInfo();
            });
            checkError(data.errorParam);
        }
    });
}

function getSlotsAjax(index = -5, value = -5, flag = false) {
    return $.ajax({
        url: 'getSlots',
        type: 'GET',
        data: {index: index, value: value, flag: flag},
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            lastTenBets = data.lastTenBets;
            coefficients = data.coefficients;
            lastSevenWinningSlots = data.lastSevenWinningSlots.map(function (number) {
                if (number === -1) {
                    return '-';
                } else {
                    return number;
                }
            });
            roundNumber = data.roundNumber;
            $("#roundNumber").text(roundNumber);
            options = data.numbers.map(function (number) {
                if (number === -1) {
                    return '';
                } else {
                    return number;
                }
            });
        }
    });
}

// function myFunction(){
//    alert('myFunction Called')
// }
//
// myFunction();
//
// setInterval(function(){
//     myFunction()}, 30000)

function checkError(error) {
    if (error === 'errorValidation') {
        $("#error").text('Incorrect bet amount');
    }
    if (error === 'errorBalance') {
        $("#error").text('Insufficient funds');
    }
    $("#amount").val('');
    setTimeout(() => $("#error").text(''), 5000);
}

function showInfo() {
    showLastSevenWinningSlots();
    showLastTenBets();
    showCoefficients();
    showBalance();
}

function showLastSevenWinningSlots() {
    table = document.querySelector('#slots tbody');
    table.innerHTML = '<tr>' +
        '<td><div class="slot-text">' + lastSevenWinningSlots[6] + '</div></td>' +
        '<td><div class="slot-text">' + lastSevenWinningSlots[5] + '</div></td>' +
        '<td><div class="slot-text">' + lastSevenWinningSlots[4] + '</div></td>' +
        '<td><div class="slot-text">' + lastSevenWinningSlots[3] + '</div></td>' +
        '<td><div class="slot-text">' + lastSevenWinningSlots[2] + '</div></td>' +
        '<td><div class="slot-text">' + lastSevenWinningSlots[1] + '</div></td>' +
        '<td><div class="slot-text">' + lastSevenWinningSlots[0] + '</div></td>' +
        '</tr>';
}

function showLastTenBets() {
    betsTable = document.querySelector('#bets tbody');
    betsTable.innerHTML = '';
    for (let i = 9; i >= 0; i--) {
        let username = lastTenBets[i].username;
        let slot = lastTenBets[i].slot;
        let coins = lastTenBets[i].coins;
        if (username === null && slot === 0) {
            slot = '-';
        }
        if (username === null) {
            username = '-';
        }
        if (coins === 0) {
            coins = '-';
        }
        betsTable.innerHTML +=
            '<tr>' +
            '<td><div class="text1">' + username + '</div></td>' +
            '<td><div class="text2">' + slot + '</div></td>' +
            '<td><div class="text3">' + coins + '</div></td>' +
            '</tr>';
    }
}

function showBalance() {
    if (document.getElementById("balance") != null) {
        getBalance();
    }
}

function showCoefficients() {
    select = document.getElementById("slot");
    if (select == null) {
        return;
    }
    select.innerHTML = '';
    coefficients.forEach(coefficient => {
        select.innerHTML += '<option value=\"' + coefficient + '\">' + coefficient + '</option>'
    });
}

$.when(getSlotsAjax()).done(function () {
    showInfo();
    drawRouletteWheel();
});

var startAngle = 0;
var spinTimeout = null;

var spinArcStart = 10;
var spinTime = 0;
var spinTimeTotal = 0;

var ctx;

document.getElementById("spin").addEventListener("click", spin);

function byte2Hex(n) {
    var nybHexString = "0123456789ABCDEF";
    return String(nybHexString.substr((n >> 4) & 0x0F, 1)) + nybHexString.substr(n & 0x0F, 1);
}

function RGB2Color(r, g, b) {
    return '#' + byte2Hex(r) + byte2Hex(g) + byte2Hex(b);
}

function getColor(item, maxitem) {
    var phase = 0;
    var center = 128;
    var width = 127;
    var frequency = Math.PI * 2 / maxitem;

    red = Math.sin(frequency * item + 2 + phase) * width + center;
    green = Math.sin(frequency * item + 0 + phase) * width + center;
    blue = Math.sin(frequency * item + 4 + phase) * width + center;

    return RGB2Color(red, green, blue);
}

function drawRouletteWheel() {
    var arc = Math.PI / (options.length / 2);
    var canvas = document.getElementById("canvas");
    if (canvas.getContext) {
        var outsideRadius = 240;
        var textRadius = 200;
        var insideRadius = 160;

        ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, 1000, 1000);

        ctx.strokeStyle = "black";
        ctx.lineWidth = 2;

        ctx.font = 'bold 12px Helvetica, Arial';

        for (var i = 0; i < options.length; i++) {
            var angle = startAngle + i * arc;
            // ctx.fillStyle = colors[i];
            if (options[i].toString() === '') {
                ctx.fillStyle = "rgba(79,66,98,0.76)";
            } else {
                ctx.fillStyle = getColor(i, options.length);
            }


            ctx.beginPath();
            ctx.arc(250, 250, outsideRadius, angle, angle + arc, false);
            ctx.arc(250, 250, insideRadius, angle + arc, angle, true);
            ctx.stroke();
            ctx.fill();

            ctx.save();
            ctx.shadowOffsetX = -1;
            ctx.shadowOffsetY = -1;
            ctx.shadowBlur = 0;
            ctx.shadowColor = "rgb(220,220,220)";
            ctx.fillStyle = "black";
            ctx.translate(250 + Math.cos(angle + arc / 2) * textRadius,
                250 + Math.sin(angle + arc / 2) * textRadius);
            ctx.rotate(angle + arc / 2 + Math.PI / 2);
            var text = options[i];
            ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
            ctx.restore();
        }

        //Arrow
        ctx.fillStyle = "black";
        ctx.beginPath();
        ctx.moveTo(250 - 4, 250 - (outsideRadius + 5));
        ctx.lineTo(250 + 4, 250 - (outsideRadius + 5));
        ctx.lineTo(250 + 4, 250 - (outsideRadius - 5));
        ctx.lineTo(250 + 9, 250 - (outsideRadius - 5));
        ctx.lineTo(250 + 0, 250 - (outsideRadius - 13));
        ctx.lineTo(250 - 9, 250 - (outsideRadius - 5));
        ctx.lineTo(250 - 4, 250 - (outsideRadius - 5));
        ctx.lineTo(250 - 4, 250 - (outsideRadius + 5));
        ctx.fill();
    }
}

function spin() {
    spinAngleStart = Math.random() * 10 + 10;
    spinTime = 0;
    spinTimeTotal = 10000;
    rotateWheel();
}

function rotateWheelDocrut() {
    var arc = Math.PI / (options.length / 2);
    startAngle += (2 * Math.PI / 180);
    var degrees = startAngle * 180 / Math.PI + 90 - (Math.floor(Math.random() * 3));
    var arcd = arc * 180 / Math.PI;
    var index = Math.floor((360 - degrees % 360) / arcd);

    if (result === options[index]) {
        $.when(getSlotsAjax(index, result, true)).done(function () {
            drawRouletteWheel();
            stopRotateWheel(index);
            showInfo();
        });
        return;
    }
    drawRouletteWheel();
    spinTimeout = setTimeout('rotateWheelDocrut()', 30);
}

function rotateWheel() {
    spinTime += 30;
    var spinAngle = spinAngleStart - easeOut(spinTime, 0, spinAngleStart, spinTimeTotal);
    startAngle += (spinAngle * Math.PI / 180);

    if (spinAngle <= 2) {
        drawRouletteWheel();
        spinTimeout = setTimeout('rotateWheelDocrut()', 30);
        return;
    }
    drawRouletteWheel();
    spinTimeout = setTimeout('rotateWheel()', 30);
}

function stopRotateWheel() {
    clearTimeout(spinTimeout);
    ctx.save();
    ctx.font = 'bold 30px Helvetica, Arial';
    var text = result;
    ctx.fillText(text, 250 - ctx.measureText(text).width / 2, 250 + 10);
    ctx.restore();
}

function easeOut(t, b, c, d) {
    var ts = (t /= d) * t;
    var tc = ts * t;
    return b + c * (tc + -3 * ts + 3 * t);
}

