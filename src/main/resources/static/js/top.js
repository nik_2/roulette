function getTopUsers() {
    return $.ajax({
        url: 'getTopUsers',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            topUsers = data.topUsers;
            showTopUsers();
        }
    });
}

getTopUsers();

function showTopUsers() {
    topTable = document.querySelector('#top-users tbody');
    topTable.innerHTML = '';
    topUsers.forEach(topUser => {
        let id = topUser.id;
        let username = topUser.username;
        let numberOfRounds = topUser.numberOfRounds;
        let averageOfBets = topUser.averageOfBets;
        if (id === 0) {
            id = '-';
        }
        if (numberOfRounds === 0 && username === null) {
            numberOfRounds = '-';
        }
        if (averageOfBets === 0 && username === null) {
            averageOfBets = '-';
        }
        if (username === null) {
            username = '-';
        }
        topTable.innerHTML +=
            '<tr>' +
            '<td class="column1"><div class="text1">' + id + '</div></td>' +
            '<td class="column2"><div class="text2">' + username + '</div></td>' +
            '<td class="column3"><div class="text3">' + numberOfRounds + '</div></td>' +
            '<td class="column4"><div class="text3">' + averageOfBets + '</div></td>' +
            '</tr>';
    });
}