function getRoundInfo() {
    return $.ajax({
        url: 'getRoundInfo',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            roundInfos = data.roundInfos;
            showRoundInfo();
        }
    });
}

getRoundInfo();

function showRoundInfo() {
    infoTable = document.querySelector('#info tbody');
    infoTable.innerHTML = '';
    for (let i = roundInfos.length - 1; i >= 0; i--) {
        let id = roundInfos[i].id;
        let numberOfSpins = roundInfos[i].spinNumber;
        let numberOfBets = roundInfos[i].betNumber;
        if (numberOfSpins === 0 && id === 0) {
            numberOfSpins = '-';
        }
        if (numberOfBets === 0 && id === 0) {
            numberOfBets = '-';
        }
        if (id === 0) {
            id = '-';
        }
        infoTable.innerHTML +=
            '<tr>' +
            '<td class="column1"><div class="text1">' + id + '</div></td>' +
            '<td class="column2"><div class="text2">' + numberOfBets + '</div></td>' +
            '<td class="column2"><div class="text2">' + numberOfSpins + '</div></td>' +
            '</tr>';
    }
}