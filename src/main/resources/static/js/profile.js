function getBalance() {
    return $.ajax({
        url: 'getBalance',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            balance = data.balance;
            $("#balance").text('Balance: ' + balance);
        }
    });
}

function getMyBets() {
    return $.ajax({
        url: 'getMyBets',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function (data) {
            myBets = data;
            getBalance();
            showMyBets();
        }
    });
}

getMyBets();

function showMyBets() {
    betsTable = document.querySelector('#bets tbody');
    betsTable.innerHTML = '';
    for (let i = myBets.length - 1; i >= 0; i--) {
        let slot = myBets[i].slotNumber;
        let coins = myBets[i].amount;
        let coefficient = myBets[i].coefficient;
        if (slot === 0 && coins === 0) {
            slot = '-';
            coins = '-';
            coefficient = '-';
        }
        betsTable.innerHTML +=
            '<tr>' +
            '<td class="column1"><div class="text1">' + slot + '</div></td>' +
            '<td class="column2"><div class="text2">' + coins + '</div></td>' +
            '<td class="column3"><div class="text3">' + coefficient + '</div></td>' +
            '</tr>';
    }
}